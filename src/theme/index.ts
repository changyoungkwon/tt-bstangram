import { extendTheme } from "@chakra-ui/react";

export default extendTheme({
  colors: {
    background: {
      secondary: "rgb(250,250,250)",
    },
  },
  sizes: {
    navbar: {
      "medium-width": "244px",
    },
  },
});
