import { atom, AtomEffect } from "recoil";
import { User } from "firebase/auth";
import { localStorageEffect } from "../utils/localStorage";

export const userAtom = atom<User | null>({
  key: "userAtom",
  default: null,
  effects: [localStorageEffect("user")],
});
