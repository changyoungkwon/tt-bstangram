import { useState } from "react";
import reactLogo from "./assets/react.svg";
import { Box, Flex } from "@chakra-ui/react";
import { Home } from "./components/Home";
import { FormModal } from "./components/Upload";
import { LoginForm } from "./components/Login";
import { useRecoilState, useRecoilValue } from "recoil";
import { createRoot } from "react-dom/client";
import { userAtom } from "./states/atoms";
import {
  createBrowserRouter,
  RouterProvider,
  Route,
  Routes,
  Link,
  redirect,
  BrowserRouter,
  Navigate,
} from "react-router-dom";

const App = () => {
  const user = useRecoilValue(userAtom);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={!user ? <Navigate to="login" /> : <Home />} />
        <Route path="login" element={<LoginForm />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
