import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination } from "swiper";
import { Box, Image, Center } from "@chakra-ui/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export enum ImageRatio {
  Square = 1,
  Landscape = 5 / 4,
  Portrait = 4 / 5,
}

interface ImageSliderProps {
  imageURLs: string[];
  imageRatio: ImageRatio;
}

export function ImageSlider(props: ImageSliderProps) {
  const swiperSliders = [];
  for (const url of props.imageURLs) {
    swiperSliders.push(
      <SwiperSlide>
        <Center style={{ aspectRatio: props.imageRatio }} bg="black">
          <Image src={url} objectFit="contain"></Image>
        </Center>
      </SwiperSlide>,
    );
  }
  return (
    <Box w="100%">
      <Swiper modules={[Navigation, Pagination]} pagination={{ clickable: true }} navigation>
        {swiperSliders}
      </Swiper>
    </Box>
  );
}
