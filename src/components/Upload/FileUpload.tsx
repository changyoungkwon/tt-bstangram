import { Center, Text, VStack, Box, Button, Flex, FormControl, Input } from "@chakra-ui/react";
import { useFormik, useFormikContext } from "formik";
import { ChangeEvent, useCallback, useRef, useEffect } from "react";
import _ from "lodash";
import { useDropzone } from "react-dropzone";
import { ReactComponent as UploadIcon } from "../../assets/upload.svg";

export function FileUpload() {
  const { setFieldValue, getFieldProps } = useFormikContext();
  const fileInputRef = useRef(null);
  // declare drop events to fill formik context
  const onDrop = useCallback((files: File[]) => {
    if (!_.isNil(files) && !_.isEmpty(files)) {
      setFieldValue("files", files);
    }
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });
  // declare button events to fill formik context
  const handleChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    if (!_.isNil(e.currentTarget.files)) {
      setFieldValue("files", [...e.currentTarget.files]);
    }
  }, []);
  const props = {
    ...getFieldProps("files"),
    onChange: handleChange,
  };
  return (
    <Center>
      <VStack alignItems="center" spacing="24px">
        <Input ref={fileInputRef} type="file" display="none" {...props} />
        <Flex {...getRootProps()} flexDir="column" alignItems="center" w="100%">
          <input {...getInputProps()} />
          <Box>
            <UploadIcon w="96px" />
          </Box>
          <Text as="h2" fontSize="22px" mt="16px">
            Drag photos and videos here
          </Text>
        </Flex>
        <Button
          borderRadius="8px"
          colorScheme="twitter"
          lineHeight="14px"
          onClick={() => fileInputRef.current?.click()}
        >
          Select from computer
        </Button>
      </VStack>
    </Center>
  );
}
