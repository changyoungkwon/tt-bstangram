export interface UploadFormValue {
  files: File[];
  caption: string;
}
