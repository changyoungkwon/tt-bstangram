import { Button, FormControl, Input, Flex } from "@chakra-ui/react";
import { Form, useFormikContext } from "formik";
import { HStack, VStack, Image } from "@chakra-ui/react";
import { useRecoilValue } from "recoil";
import { userAtom } from "../../states/atoms";
import { useEffect, useState } from "react";
import _ from "lodash";

import { UploadFormValue } from "./interface";

export function PostUpload() {
  const { getFieldProps, isSubmitting, values } = useFormikContext<UploadFormValue>();
  const user = useRecoilValue(userAtom);
  const [preview, setPreview] = useState<string | undefined>();
  useEffect(() => {
    if (!_.isNil(values.files) && !_.isEmpty(values.files)) {
      const objectUrl = URL.createObjectURL(values.files[0]);
      setPreview(objectUrl);
      return () => URL.revokeObjectURL(objectUrl);
    }
  }, [values.files]);
  return (
    <Form>
      <HStack>
        <Image src={preview}></Image>
        {/* <VStack w="340px"> */}
        {/*   <Flex flexDir="row"> */}
        {/*     <Image></Image> */}
        {/*     <span></span> */}
        {/*   </Flex> */}
        {/*   <Input type="text" {...getFieldProps("caption")} /> */}
        {/*   <Flex flexDir="row"> */}
        {/*     <Image></Image> */}
        {/*     <span></span> */}
        {/*   </Flex> */}
        {/*   <Button type="submit" isLoading={isSubmitting}> */}
        {/*     Share */}
        {/*   </Button> */}
        {/* </VStack> */}
      </HStack>
    </Form>
  );
}
