import { object, string, mixed } from "yup";
import { useEffect, useMemo } from "react";
import { Formik, FormikProps, FormikValues } from "formik";
import { Divider, useDisclosure } from "@chakra-ui/react";
import _ from "lodash";
import { FileUpload } from "./FileUpload";
import { PostUpload } from "./PostUpload";
import { uploadFile } from "../../services/storage";
import {
  Center,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
} from "@chakra-ui/react";
import { useRecoilValue } from "recoil";
import { userAtom } from "../../states/atoms/user";
import { UploadFormValue } from "./interface";

export function FormModal() {
  const formSchema = useMemo(() => {
    return object({
      files: mixed().required(),
      password: string().notRequired(),
    });
  }, []);

  const user = useRecoilValue(userAtom);

  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Formik
      initialValues={{ files: [], caption: "" }}
      validationSchema={formSchema}
      validateOnMount={true}
      onSubmit={(values: UploadFormValue, { setSubmitting, setValues }) => {
        if (_.isNil(values.files) || _.isNil(user)) {
          setSubmitting(false);
          return;
        }
        Promise.all(
          (values.files as Array<File>).map(async (f) => {
            try {
              const url = await uploadFile(f, user?.uid, `test/${f.name}`);
              console.log("success: " + url);
            } catch (err) {
              console.log(err);
            }
          }),
        ).then(() => {
          setSubmitting(false);
        });
      }}
    >
      {(formik) => {
        const filesEmpty = _.isEmpty(formik.values.files);
        return (
          <>
            <Button onClick={onOpen}>Open Modal</Button>
            <Modal
              isOpen={isOpen}
              onClose={() => {
                onClose();
                formik.setValues(formik.initialValues);
              }}
            >
              <ModalOverlay />
              <ModalContent w="425px" maxW="min( calc( 100vw - 372px ), 855px )" minW="348px">
                <ModalHeader>
                  <Center>Create new post</Center>
                </ModalHeader>
                <Divider color="black" />
                <ModalBody h="calc( 100vmin - 219px )" maxH="min( calc( 100vw - 372px ), 855px )" minH="348px">
                  {filesEmpty ? <FileUpload /> : <PostUpload />}
                </ModalBody>
              </ModalContent>
            </Modal>
          </>
        );
      }}
    </Formik>
  );
}
