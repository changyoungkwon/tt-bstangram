import { VStack, Box, Image, Link } from "@chakra-ui/react";
import { PostHeader, PostHeaderProps } from "./PostHeader";
import profileUrl from "../../assets/profile.jpg";
import ignitoUrl from "../../assets/ignito.jpg";
import { ImageSlider, ImageRatio } from "../common";
import { IconStack } from "./IconStack";
import { NewComment } from "./NewComment";
export interface PostProps {
  // profile of author
  username: string;
  name: string;
  profilePhoto: string;
  // contents and photos
  photos: string[];
  ratio: ImageRatio;
  content: string;
  // likes
  isLiked: boolean;
  likeCounts: boolean;
  // comments
  commentCounts: number;
  // other props
  isFolded: boolean;
}

export function Post(/*props: PostProps*/) {
  let props = {
    username: "changyoung",
    name: "changyoung.kwon",
    profilePhoto: profileUrl,
  };
  let isProps = {
    imageURLs: [profileUrl, profileUrl, ignitoUrl],
    imageRatio: ImageRatio.Portrait,
  };

  return (
    <VStack maxWidth="470px">
      <PostHeader username={props.username} name={props.name} profilePhoto={props.profilePhoto}></PostHeader>
      <ImageSlider imageURLs={isProps.imageURLs} imageRatio={isProps.imageRatio} />
      <VStack w="100%" px="12px" pb="6px" align="stretch">
        <IconStack isLiked={true} isSaved={true} />
        <Box textAlign="left">{`{likeCounts} likes`}</Box>
        <Box textAlign="left">{}</Box>
        <Link textAlign="left" href="#">
          View all comments
        </Link>
        <Box textAlign="left">2 days ago</Box>
        <NewComment />
      </VStack>
    </VStack>
  );
}
