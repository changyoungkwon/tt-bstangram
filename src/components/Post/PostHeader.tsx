import { Box, Flex, Icon, IconButton, Image, HStack, Link, VStack, Spacer } from "@chakra-ui/react";
import { MdMoreHoriz } from "react-icons/md";
import fallbackImageUrl from "../../assets/ignito.jpg";

export interface PostHeaderProps {
  username: string;
  name: string;
  profilePhoto: string;
}
export function PostHeader(props: PostHeaderProps) {
  return (
    <Flex dir="row" w="100%" alignItems="center">
      <HStack spacing="12px" my="8px" mr="4px" ml="12px" alignItems="center" flexGrow={1}>
        <Link href="#">
          <Image
            boxSize="32px"
            borderRadius="full"
            objectFit="cover"
            fallbackSrc="fallbackImageUrl"
            src={props.profilePhoto}
            alt={props.username + "'s profile photo"}
          ></Image>
        </Link>
        <Box textAlign="left">
          <Link href="#" _hover={{ color: "black" }}>
            <strong>{props.username}</strong>
          </Link>
        </Box>
      </HStack>
      <Box p="8px" mr="4px">
        <IconButton
          aria-label="more"
          bg="white"
          _hover={{ borderColor: "white" }}
          icon={<Icon as={MdMoreHoriz} w="24px" h="24px" />}
        ></IconButton>
      </Box>
    </Flex>
  );
}
