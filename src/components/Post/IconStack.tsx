import { Flex, Icon, Spacer, IconButton } from "@chakra-ui/react";
import {
  AiOutlineHeart,
  AiFillHeart,
  AiOutlineShareAlt,
  AiOutlineComment,
  AiOutlineSave,
  AiFillSave,
} from "react-icons/ai";

interface IconStackProps {
  isLiked: boolean;
  isSaved: boolean;
}

export function IconStack(props: IconStackProps) {
  return (
    <Flex as="section" dir="row" mt="4px">
      <IconButton
        w="40px"
        h="40px"
        bg="white"
        ml="-8px"
        _hover={{ borderColor: "white" }}
        aria-label="like"
        icon={<Icon w="24px" h="24px" as={AiOutlineHeart} />}
      />
      <IconButton
        w="40px"
        h="40px"
        bg="white"
        _hover={{ borderColor: "white" }}
        aria-label="comment"
        icon={<Icon w="24px" h="24px" as={AiOutlineComment} />}
      />
      <IconButton
        w="40px"
        h="40px"
        bg="white"
        _hover={{ borderColor: "white" }}
        aria-label="share"
        icon={<Icon w="24px" h="24px" as={AiOutlineShareAlt} />}
      />
      <Spacer />
      <IconButton
        w="40px"
        h="40px"
        bg="white"
        mr="-8px"
        _hover={{ borderColor: "white" }}
        aria-label="save"
        icon={<Icon w="24px" h="24px" as={AiOutlineSave} />}
      />
    </Flex>
  );
}
