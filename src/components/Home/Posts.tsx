import { Post } from "../Post";
import { VStack } from "@chakra-ui/react";

export function Posts() {
  return (
    <VStack>
      <Post />
      <Post />
      <Post />
    </VStack>
  );
}
