import { useEffect } from "react";
import { useRecoilValue } from "recoil";
import { useNavigate } from "react-router-dom";
import { userAtom } from "../../states/atoms";
import { Posts } from "./Posts";
import { Sidebar } from "./Sidebar";
import { Center, Flex, HStack } from "@chakra-ui/react";
import { MyProfile } from "./Profile";

export const Home = () => {
  const user = useRecoilValue(userAtom);
  const navigate = useNavigate();
  useEffect(() => {
    if (!user) {
      navigate("/login");
    }
  }, [user]);
  return (
    <Flex w="100vw" bg="background.secondary">
      <Sidebar />
      <Center h="100vh" w="calc(100% - var(--chakra-sizes-navbar-medium-width))" ml="auto">
        <HStack>
          <Posts />
          <MyProfile />
        </HStack>
      </Center>
    </Flex>
  );
};
