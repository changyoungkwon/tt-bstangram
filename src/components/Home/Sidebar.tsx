import { Box, HStack, Flex, Text, Icon, Image } from "@chakra-ui/react";
import { MdHome, MdOutlineAddBox, MdOutlineFavoriteBorder } from "react-icons/md";
import ignito from "@/assets/ignito.jpg";
import { ReactComponent as InstaLogo } from "@/assets/logo.svg";

export function Sidebar() {
  return (
    <Flex
      flexDir="column"
      pos="fixed"
      h="100vh"
      w="navbar.medium-width"
      fontSize="14px"
      px="12px"
      pt="8px"
      pb="20px"
      alignItems="flex-start"
      background="transparent"
      borderRight="1px solid rgb(219,219,219)"
    >
      <Box mb="19px" px="12px" pt="25px" pb="16px" h="73px">
        <Box mt="7px">
          <InstaLogo />
        </Box>
      </Box>
      <HStack p="12px" my="4px" spacing="16px">
        <Icon as={MdHome} w="24px" h="24px" />
        <Text as="b">Home</Text>
      </HStack>
      <HStack p="12px" my="4px" spacing="16px">
        <Icon as={MdOutlineFavoriteBorder} w="24px" h="24px" />
        <Text>Notifications</Text>
      </HStack>
      <HStack p="12px" my="4px" spacing="16px">
        <Icon as={MdOutlineAddBox} w="24px" h="24px" />
        <Text>Create</Text>
      </HStack>
      <HStack p="12px" my="4px" spacing="16px">
        <Image borderRadius="full" src={ignito} w="24px" h="24px" />
        <Text>Profile</Text>
      </HStack>
    </Flex>
  );
}
