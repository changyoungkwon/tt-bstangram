import { Formik, Field, Form, ErrorMessage, useField, useFormikContext } from "formik";
import { useRecoilState } from "recoil";
import { redirect, useNavigate } from "react-router-dom";
import { userAtom } from "../../states/atoms";
import { FormControl, FormLabel, Input, Button, FormErrorMessage } from "@chakra-ui/react";
import { signin } from "../../services/auth";
import { useEffect, useMemo } from "react";
import { User } from "firebase/auth";
import { object, string } from "yup";

const TextInput = ({ name, label, ...props }) => {
  const [field] = useField({ name });
  return (
    <FormControl>
      <FormLabel>{label}</FormLabel>
      <Input {...field} {...props} />
      {/* <FormErrorMessage>{meta.error}</FormErrorMessage> */}
    </FormControl>
  );
};

const ButtonInput = (props: any) => {
  const { isSubmitting, isValid, isInitialValid } = useFormikContext();
  return (
    <Button isLoading={isSubmitting} type="submit" isDisabled={!isValid} {...props}>
      Submit
    </Button>
  );
};

const SubmitErrorMessage = (props: any) => {
  const [field] = useField({ name: "error" });
  return (
    <FormControl isInvalid={field.value === "invalid"}>
      <Input type="hidden" name="error" />
      <FormErrorMessage>Sorry, your password was incorrect. Please double-check your password.</FormErrorMessage>
    </FormControl>
  );
};

export function LoginForm() {
  const [user, setUser] = useRecoilState(userAtom);
  const navigate = useNavigate();
  const formSchema = useMemo(() => {
    return object({
      username: string().required().min(1),
      password: string().required().min(6),
    });
  }, []);
  useEffect(() => {
    if (user) {
      navigate("/");
    }
  }, [user]);

  return (
    <Formik
      initialValues={{ username: "", password: "" }}
      validationSchema={formSchema}
      isInitialValid={false}
      onSubmit={(values, actions) => {
        signin(values.username, values.password)
          .then((user: User | undefined) => {
            if (user) {
              actions.setFieldValue("error", "invalid");
              setUser(user);
            } else {
              actions.setFieldValue("error", "");
            }
          })
          .catch(() => {
            actions.setFieldValue("error", "invalid");
          })
          .finally(() => {
            actions.setSubmitting(false);
          });
      }}
    >
      <Form>
        <TextInput label="User Name" name="username" type="text" />
        <TextInput label="Password" name="password" type="password" />
        <ButtonInput />
        <SubmitErrorMessage />
      </Form>
    </Formik>
  );
}
