// declare entities based on screen
export interface Post {
  id: string;
  user: User;
  content: string;
  imageUrls: string[];
  imageRatio: string;
  createdAt: Date;
  likes: Like[];
  replies: Reply[];
}

export interface Like {
  post_or_reply_id: string;
  user: User;
}

export interface Reply {
  post_id: string;
  user: User;
  contents: string;
  createdAt: Date;
  likes: Like[];
}

export interface User {
  id: string;
  username: string;
  name: string;
  profilePhotoUrl: string;
}

// entities for api request
export interface CreatePostReq {
  user_id: string;
  content: string;
  imageUrls: string[];
  imageRatio: string;
}

export interface LikePostReq {
  user_id: string;
  post_id: string;
}

export interface UnlikePostReq {
  user_id: string;
  post_id: string;
}

export interface ReplyPostReq {
  user_id: string;
  post_id: string;
  contents: string;
}

export interface LikeReplyReq {
  post_id: string;
  reply_id: string;
  user_id: string;
}
