import { CreatePostReq, LikePostReq, LikeReplyReq, Post, ReplyPostReq, UnlikePostReq } from "./interface";
import {
  doc,
  setDoc,
  getDoc,
  getFirestore,
  query,
  where,
  Timestamp,
  collection,
  addDoc,
  getDocs,
  deleteDoc,
} from "firebase/firestore";
import { initFirebase } from "./firebase";

const app = initFirebase();
const db = getFirestore(app);

export async function getPosts(user_id: string): Promise<Post[]> {
  const postQuery = query(collection(db, "post"), where("user_id", "==", user_id));
  const postSnapshot = await getDocs(postQuery);
  const userQuery = query(collection(db, "user"), where("user_id", "in", []));
  throw "not implemented yet";
}

export async function createPost(post: CreatePostReq): Promise<void> {
  const colRef = collection(db, "post");
  const docRef = await addDoc(colRef, {
    ...post,
    createdAt: Timestamp.fromDate(new Date()),
  });
  console.log(docRef);
}

export async function likePost(req: LikePostReq): Promise<void> {
  const docRef = doc(db, "post", req.post_id, "like", req.user_id);
  await setDoc(docRef, {
    ...req,
    createdAt: Timestamp.fromDate(new Date()),
  });
}

export async function unlikePost(req: UnlikePostReq): Promise<void> {
  const docRef = doc(db, "post", req.post_id, "like", req.user_id);
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    await deleteDoc(docRef);
  }
}

export async function replyPost(req: ReplyPostReq): Promise<void> {
  const colRef = collection(db, "post", req.post_id, "reply");
  await addDoc(colRef, {
    ...req,
    createdAt: Timestamp.fromDate(new Date()),
  });
}

export async function likeReply(req: LikeReplyReq): Promise<void> {
  const colRef = collection(db, "post", req.post_id, "reply", req.reply_id, "like");
  await addDoc(colRef, {
    ...req,
    createdAt: Timestamp.fromDate(new Date()),
  });
}
