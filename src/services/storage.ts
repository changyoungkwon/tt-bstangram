import { getStorage, ref, getDownloadURL, uploadBytes } from "firebase/storage";

export async function uploadFile(file: File, uid: string, key: string): Promise<string> {
  // create a reference to key
  try {
    const fileRef = ref(getStorage(), `/user/${uid}/${key}`);
    const buffer = await file.arrayBuffer();
    const snapshot = await uploadBytes(fileRef, buffer, {});
    const downloadURL = await getDownloadURL(snapshot.ref);
    return downloadURL;
  } catch (res) {
    console.log(`error: ${JSON.stringify(res, null, 2)}`);
    throw res;
  }
}
