// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: "AIzaSyBX3yj4HLG5PY3QLx1srrMmi3ik-UG33CA",
  authDomain: "bstangram.firebaseapp.com",
  projectId: "bstangram",
  storageBucket: "bstangram.appspot.com",
  messagingSenderId: "197353108869",
  appId: "1:197353108869:web:9822e033fae66b1e7880ec",
  measurementId: "G-WF14BZJD81",
};

// Initialize Firebase
export function initFirebase() {
  const app = initializeApp(firebaseConfig);
  getAnalytics(app);
}
