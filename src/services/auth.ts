import { getAuth, signInWithEmailAndPassword, User, setPersistence, browserLocalPersistence } from "firebase/auth";
import { Auth } from "firebase/auth";

export function initAuth(): Auth {
  const auth = getAuth();
  setPersistence(auth, browserLocalPersistence);
  return auth;
}

export async function signin(email: string, password: string): Promise<User | undefined> {
  const auth = initAuth();
  try {
    const userCredentials = await signInWithEmailAndPassword(auth, email, password);
    return userCredentials.user;
  } catch (err: any) {
    // throw { code: err.code, message: err.message };
    throw err;
  }
}
