import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { initFirebase } from "./services/firebase";
import { RecoilRoot, useRecoilState, useRecoilValue } from "recoil";
import { ChakraProvider } from "@chakra-ui/react";
import theme from "./theme";

initFirebase();
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RecoilRoot>
      <ChakraProvider theme={theme}>
        <App />
      </ChakraProvider>
    </RecoilRoot>
  </React.StrictMode>,
);
